<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Teachers Class'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Teachers'), ['controller' => 'Teachers', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Teacher'), ['controller' => 'Teachers', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Teams'), ['controller' => 'Teams', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Team'), ['controller' => 'Teams', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="teachersClasses index large-9 medium-8 columns content">
    <h3><?= __('Teachers Classes') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('teacher_id') ?></th>
                <th><?= $this->Paginator->sort('team_id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($teachersClasses as $teachersClass): ?>
            <tr>
                <td><?= $teachersClass->has('teacher') ? $this->Html->link($teachersClass->teacher->id, ['controller' => 'Teachers', 'action' => 'view', $teachersClass->teacher->id]) : '' ?></td>
                <td><?= $teachersClass->has('team') ? $this->Html->link($teachersClass->team->id, ['controller' => 'Teams', 'action' => 'view', $teachersClass->team->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $teachersClass->teacher_id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $teachersClass->teacher_id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $teachersClass->teacher_id], ['confirm' => __('Are you sure you want to delete # {0}?', $teachersClass->teacher_id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
