<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Teachers Class'), ['action' => 'edit', $teachersClass->teacher_id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Teachers Class'), ['action' => 'delete', $teachersClass->teacher_id], ['confirm' => __('Are you sure you want to delete # {0}?', $teachersClass->teacher_id)]) ?> </li>
        <li><?= $this->Html->link(__('List Teachers Classes'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Teachers Class'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Teachers'), ['controller' => 'Teachers', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Teacher'), ['controller' => 'Teachers', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Teams'), ['controller' => 'Teams', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Team'), ['controller' => 'Teams', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="teachersClasses view large-9 medium-8 columns content">
    <h3><?= h($teachersClass->teacher_id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Teacher') ?></th>
            <td><?= $teachersClass->has('teacher') ? $this->Html->link($teachersClass->teacher->id, ['controller' => 'Teachers', 'action' => 'view', $teachersClass->teacher->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Team') ?></th>
            <td><?= $teachersClass->has('team') ? $this->Html->link($teachersClass->team->id, ['controller' => 'Teams', 'action' => 'view', $teachersClass->team->id]) : '' ?></td>
        </tr>
    </table>
</div>
