<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit School'), ['action' => 'edit', $school->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete School'), ['action' => 'delete', $school->id], ['confirm' => __('Are you sure you want to delete # {0}?', $school->id)]) ?> </li>
        <li><?= $this->Html->link(__('List School'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New School'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Unit'), ['controller' => 'Unit', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Unit'), ['controller' => 'Unit', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="school view large-9 medium-8 columns content">
    <h3><?= h($school->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Nome') ?></th>
            <td><?= h($school->nome) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($school->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($school->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Mofied') ?></th>
            <td><?= h($school->mofied) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Unit') ?></h4>
        <?php if (!empty($school->unit)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('School Id') ?></th>
                <th><?= __('Nome') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Mofied') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($school->unit as $unit): ?>
            <tr>
                <td><?= h($unit->id) ?></td>
                <td><?= h($unit->school_id) ?></td>
                <td><?= h($unit->nome) ?></td>
                <td><?= h($unit->created) ?></td>
                <td><?= h($unit->mofied) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Unit', 'action' => 'view', $unit->id]) ?>

                    <?= $this->Html->link(__('Edit'), ['controller' => 'Unit', 'action' => 'edit', $unit->id]) ?>

                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Unit', 'action' => 'delete', $unit->id], ['confirm' => __('Are you sure you want to delete # {0}?', $unit->id)]) ?>

                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
    </div>
</div>
