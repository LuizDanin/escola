<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * TeachersClasses Controller
 *
 * @property \App\Model\Table\TeachersClassesTable $TeachersClasses
 */
class TeachersClassesController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Teachers', 'Teams']
        ];
        $this->set('teachersClasses', $this->paginate($this->TeachersClasses));
        $this->set('_serialize', ['teachersClasses']);
    }

    /**
     * View method
     *
     * @param string|null $id Teachers Class id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $teachersClass = $this->TeachersClasses->get($id, [
            'contain' => ['Teachers', 'Teams']
        ]);
        $this->set('teachersClass', $teachersClass);
        $this->set('_serialize', ['teachersClass']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $teachersClass = $this->TeachersClasses->newEntity();
        if ($this->request->is('post')) {
            $teachersClass = $this->TeachersClasses->patchEntity($teachersClass, $this->request->data);
            if ($this->TeachersClasses->save($teachersClass)) {
                $this->Flash->success(__('The teachers class has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The teachers class could not be saved. Please, try again.'));
            }
        }
        $teachers = $this->TeachersClasses->Teachers->find('list', ['limit' => 200]);
        $teams = $this->TeachersClasses->Teams->find('list', ['limit' => 200]);
        $this->set(compact('teachersClass', 'teachers', 'teams'));
        $this->set('_serialize', ['teachersClass']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Teachers Class id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $teachersClass = $this->TeachersClasses->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $teachersClass = $this->TeachersClasses->patchEntity($teachersClass, $this->request->data);
            if ($this->TeachersClasses->save($teachersClass)) {
                $this->Flash->success(__('The teachers class has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The teachers class could not be saved. Please, try again.'));
            }
        }
        $teachers = $this->TeachersClasses->Teachers->find('list', ['limit' => 200]);
        $teams = $this->TeachersClasses->Teams->find('list', ['limit' => 200]);
        $this->set(compact('teachersClass', 'teachers', 'teams'));
        $this->set('_serialize', ['teachersClass']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Teachers Class id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $teachersClass = $this->TeachersClasses->get($id);
        if ($this->TeachersClasses->delete($teachersClass)) {
            $this->Flash->success(__('The teachers class has been deleted.'));
        } else {
            $this->Flash->error(__('The teachers class could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
