-- phpMyAdmin SQL Dump
-- version 4.2.12deb2+deb8u1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Tempo de geração: 19/11/2015 às 02:31
-- Versão do servidor: 5.5.46-0+deb8u1
-- Versão do PHP: 5.6.14-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Banco de dados: `escola`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `disciplines`
--

CREATE TABLE IF NOT EXISTS `disciplines` (
`id` int(11) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Fazendo dump de dados para tabela `disciplines`
--

INSERT INTO `disciplines` (`id`, `nome`, `created`, `modified`) VALUES
(1, 'Matemática', '2015-11-19 04:21:20', '2015-11-19 04:21:20'),
(2, 'Inglês', '2015-11-19 04:21:31', '2015-11-19 04:21:31'),
(3, 'Portugês', '2015-11-19 04:21:44', '2015-11-19 04:21:44'),
(4, 'Educação Física', '2015-11-19 04:22:05', '2015-11-19 04:22:05'),
(5, 'Informática', '2015-11-19 04:24:52', '2015-11-19 04:25:42');

-- --------------------------------------------------------

--
-- Estrutura para tabela `schools`
--

CREATE TABLE IF NOT EXISTS `schools` (
`id` int(11) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Fazendo dump de dados para tabela `schools`
--

INSERT INTO `schools` (`id`, `nome`, `created`, `modified`) VALUES
(1, 'Mosaico', '2015-11-19 02:02:53', '2015-11-19 02:02:00');

-- --------------------------------------------------------

--
-- Estrutura para tabela `students`
--

CREATE TABLE IF NOT EXISTS `students` (
`id` int(11) NOT NULL,
  `team_id` int(11) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Fazendo dump de dados para tabela `students`
--

INSERT INTO `students` (`id`, `team_id`, `nome`, `created`, `modified`) VALUES
(1, 1, 'Luiz Danin', '2015-11-19 04:01:20', '2015-11-19 04:01:20'),
(2, 2, 'Val Cunha', '2015-11-19 04:01:47', '2015-11-19 04:01:47'),
(3, 1, 'Otavio Lima', '2015-11-19 04:03:15', '2015-11-19 04:03:15'),
(4, 2, 'Valdicléa Pantoja', '2015-11-19 04:03:54', '2015-11-19 04:04:06'),
(5, 3, 'Ana Carla', '2015-11-19 04:04:29', '2015-11-19 04:04:29'),
(6, 4, 'João Gilberto', '2015-11-19 04:04:50', '2015-11-19 04:04:50');

-- --------------------------------------------------------

--
-- Estrutura para tabela `teachers`
--

CREATE TABLE IF NOT EXISTS `teachers` (
`id` int(11) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `discipline_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Fazendo dump de dados para tabela `teachers`
--

INSERT INTO `teachers` (`id`, `nome`, `created`, `modified`, `discipline_id`) VALUES
(1, 'Val Cunha Pantoja', '2015-11-19 04:24:01', '2015-11-19 04:24:01', 1),
(2, 'Mário Cálculo', '2015-11-19 04:25:58', '2015-11-19 04:25:58', 1),
(3, 'Luiz Danin de Lima', '2015-11-19 04:26:20', '2015-11-19 04:26:20', 5);

-- --------------------------------------------------------

--
-- Estrutura para tabela `teachers_classes`
--

CREATE TABLE IF NOT EXISTS `teachers_classes` (
  `teacher_id` int(11) NOT NULL,
  `team_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura para tabela `teams`
--

CREATE TABLE IF NOT EXISTS `teams` (
`id` int(11) NOT NULL,
  `unit_id` int(11) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Fazendo dump de dados para tabela `teams`
--

INSERT INTO `teams` (`id`, `unit_id`, `nome`, `created`, `modified`) VALUES
(1, 3, 'Marex 1º ano', '2015-11-19 03:58:50', '2015-11-19 03:58:50'),
(2, 4, 'Promorá 1º ano', '2015-11-19 03:59:10', '2015-11-19 03:59:56'),
(3, 3, 'Marex 2º ano', '2015-11-19 03:59:28', '2015-11-19 03:59:28'),
(4, 4, 'Promorá 2º ano', '2015-11-19 03:59:46', '2015-11-19 03:59:46');

-- --------------------------------------------------------

--
-- Estrutura para tabela `units`
--

CREATE TABLE IF NOT EXISTS `units` (
`id` int(11) NOT NULL,
  `school_id` int(11) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Fazendo dump de dados para tabela `units`
--

INSERT INTO `units` (`id`, `school_id`, `nome`, `created`, `modified`) VALUES
(3, 1, 'Mosaico Marex', '2015-11-19 03:56:38', '2015-11-19 03:56:38'),
(4, 1, 'Mosaico Promorá', '2015-11-19 03:56:45', '2015-11-19 03:56:45');

--
-- Índices de tabelas apagadas
--

--
-- Índices de tabela `disciplines`
--
ALTER TABLE `disciplines`
 ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `schools`
--
ALTER TABLE `schools`
 ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `students`
--
ALTER TABLE `students`
 ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `teachers`
--
ALTER TABLE `teachers`
 ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `teachers_classes`
--
ALTER TABLE `teachers_classes`
 ADD PRIMARY KEY (`teacher_id`,`team_id`);

--
-- Índices de tabela `teams`
--
ALTER TABLE `teams`
 ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `units`
--
ALTER TABLE `units`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de tabelas apagadas
--

--
-- AUTO_INCREMENT de tabela `disciplines`
--
ALTER TABLE `disciplines`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de tabela `schools`
--
ALTER TABLE `schools`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de tabela `students`
--
ALTER TABLE `students`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de tabela `teachers`
--
ALTER TABLE `teachers`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de tabela `teams`
--
ALTER TABLE `teams`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de tabela `units`
--
ALTER TABLE `units`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
