<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TeachersClassesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TeachersClassesTable Test Case
 */
class TeachersClassesTableTest extends TestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.teachers_classes',
        'app.teachers',
        'app.disciplines',
        'app.teams',
        'app.units',
        'app.schools',
        'app.students'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('TeachersClasses') ? [] : ['className' => 'App\Model\Table\TeachersClassesTable'];
        $this->TeachersClasses = TableRegistry::get('TeachersClasses', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TeachersClasses);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
